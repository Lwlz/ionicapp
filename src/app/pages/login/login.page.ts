import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {
 
  email: string = '';
  password: string = '';

  constructor(private authService: AuthenticationService) { }
 
  
  ngOnInit() {
    console.log('the username', this.email);
    console.log(this.password);
  }
 
  login() {
    this.authService.login();
}

}