import { AuthGuard } from './guards/auth.guard';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  // { path: 'api', canActivate: [AuthGuard], loadChildren: './tabs/tabs.module#TabsPageModule' },
  // { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  // { path: '', redirectTo: 'login', pathMatch: 'full' },
  
  { path: '', redirectTo: 'news', pathMatch: 'full' },
  { path: 'news', loadChildren: './pages/news/news.module#NewsPageModule' },
  { path: 'news/:id', loadChildren: './pages/news-details/news-details.module#NewsDetailsPageModule' },
  { path: 'addnews', loadChildren: './pages/addnews/addnews.module#AddnewsPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'profile', loadChildren: './pages/profile/profile.module#ProfilePageModule' },
  
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
